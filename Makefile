FC = gfortran
FFLAGS = -fbackslash

.PHONY: clean

write.x: main.f08
	$(FC) $(FFLAGS) -o write.x main.f08

clean:
	rm *.x *.vtk
